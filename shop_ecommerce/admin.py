from django.contrib import admin
from .models import *


# Register your models here.
class subcategoryTabularInline(admin.TabularInline):
    model = SubCategory


class categoryAdmin(admin.ModelAdmin):
    inlines = [subcategoryTabularInline]


class ProducthasimageTabularInline(admin.TabularInline):
    model = ProductHasImage


class ProductAdmin(admin.ModelAdmin):
    inlines = [ProducthasimageTabularInline]


# admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductReview)
admin.site.register(Cart)
admin.site.register(Signup)
admin.site.register(Category,categoryAdmin)
