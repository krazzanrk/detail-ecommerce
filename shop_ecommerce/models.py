import math

from django.contrib.auth.models import User
import requests as req
from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField
from autoslug import AutoSlugField


# Create your models here.

class Category(models.Model):
    title = models.CharField(max_length=25)
    slug = AutoSlugField(populate_from='title', unique=True)

    def __str__(self):
        return self.title

    def image(self):
        return self.subcategory_set.first()


class SubCategory(models.Model):
    title = models.CharField(max_length=25)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    slug = AutoSlugField(populate_from='title', unique=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=80)
    description = RichTextField()
    brand = models.CharField(max_length=25)
    price = models.FloatField()
    discount = models.FloatField()
    pub_date = models.DateTimeField(auto_now=True)
    slug = AutoSlugField(populate_from='title', unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def image(self):
        return self.producthasimage_set.first()

    def avg_rating(self):
        result = self.productreview_set.aggregate(models.Avg('rating'))  # finding average of rating
        if result['rating__avg'] == None:
            result['rating__avg'] = 0
        return math.ceil(result['rating__avg'])

    def money_conversion(self):
        current_rate = req.get('http://free.currencyconverterapi.com/api/v5/convert?q=USD_NPR&compact=y').json()['USD_NPR']['val']
        mo = current_rate * self.price
        print('Amount in NPR :', mo)
        return mo


class ProductHasImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = ImageField()


class ProductReview(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    rating = models.CharField(max_length=20)
    comment = models.TextField()
    pub_date = models.DateTimeField(auto_now=True)


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    qty = models.IntegerField()
    pub_date = models.DateTimeField(auto_now=True)

    def total(self):
        return self.product.price * self.qty


class Signup(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    password = models.TextField(max_length=128)
    confirm_password = models.TextField(max_length=128)


# class User(...):
#     role = models.CharField(choices=(
#         ('Doctor', 'Doctor'),
#         ('Patient', 'Patient')
#     ))