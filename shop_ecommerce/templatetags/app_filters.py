from django import template

register = template.Library()


def times(n):
    return range(int(n))


register.filter('range', times)
