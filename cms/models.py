from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField
from autoslug import AutoSlugField


# Create your models here.

class Banner(models.Model):
    title = models.CharField(max_length=25)
    content = RichTextField()
    image = ImageField()
    weight = models.IntegerField()
    published = models.BooleanField()

    def __str__(self):
        return str(self.weight)


class Page(models.Model):
    title = models.CharField(max_length=25)
    content = RichTextField()
    image = ImageField()
    published = models.BooleanField()
    navbar = models.BooleanField()
    slug = AutoSlugField(populate_from='title', unique=True)

    def __str__(self):
        return self.title




