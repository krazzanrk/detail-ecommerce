from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.views import View
from django.views.generic import ListView

from shop_ecommerce.form import CommentForm, SignUpForm, SignupForm
from shop_ecommerce.models import *
from cms.models import *


class BaseView(View):
    template_context = {
        'categories': Category.objects.order_by('title').all(),
        'subcategories': SubCategory.objects.all(),
        'navbar_pages': Page.objects.filter(navbar=True),
        'totals': Cart.objects.all()

    }


class HomePage(BaseView):
    def get(self, request):
        self.template_context['deals'] = Product.objects.order_by('-id')[:1]
        self.template_context['daydeals'] = Product.objects.order_by('?')[:6]
        self.template_context['latest'] = Product.objects.order_by('pub_date')[:4]
        self.template_context['toprated'] = Product.objects.order_by('?')[:4]
        self.template_context['choose'] = Product.objects.order_by('?')[:4]
        self.template_context['banners'] = Banner.objects.filter(published=True).order_by('-weight')
        return render(request, 'index.html', self.template_context)


class SubCategoryView(BaseView):
    # model = Product
    # # queryset = SubCategory.objects.get(s).product_set.all()
    # template_name = 'products.html'
    # context_object_name = 'subcategory_pages'
    # paginate_by = 9
    #
    # # ordering = 'title'
    #
    # def get_queryset(self, *args, **kwargs):
    #     brand = self.request.GET.get('brand', None)
    #     if brand is None:
    #         queryset = super().get_queryset(*args, **kwargs)
    #     else:
    #         queryset = self.model.objects.filter(brand__iexact=brand)
    #     return queryset
    #
    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     context['brands'] = Product.objects.all().values_list('brand', flat=True).distinct()
    #     context.update({
    #         'categories': Category.objects.order_by('title').all(),
    #         'subcategories': SubCategory.objects.all(),
    #         'navbar_pages': Page.objects.filter(navbar=True),
    #         'totals': Cart.objects.all()
    #     })
    #     return context
    # #



    def get(self, request, sub_slug):
        self.template_context['subcategory'] = SubCategory.objects.get(slug=sub_slug).product_set.all()
        subcategory_product_list = SubCategory.objects.get(slug=sub_slug).product_set.all()
        paginator = Paginator(subcategory_product_list, 9)
        page = request.GET.get('page', 1)
        self.template_context['subcategory_pages'] = paginator.get_page(page)
        self.template_context['paginator'] = paginator

        return render(request, 'products.html', self.template_context)




class AllProduct(BaseView):
    def get(self, request):
        self.template_context['products'] = Product.objects.all()
        all_product_list = Product.objects.all()
        paginator = Paginator(all_product_list, 9)
        page = request.GET.get('page', 1)
        self.template_context['allproduct_pages'] = paginator.get_page(page)
        self.template_context['paginator'] = paginator
        return render(request, 'all_product.html', self.template_context)


class AllProduct(ListView):
    model = Product
    # queryset = Product.objects.filter(is_active=True)
    template_name = 'all_product.html'
    context_object_name = 'allproduct_pages'
    paginate_by = 9

    # ordering = 'title'

    def get_queryset(self, *args, **kwargs):
        brand = self.request.GET.get('brand', None)
        if brand is None:
            queryset = super().get_queryset(*args, **kwargs)
        else:
            queryset = self.model.objects.filter(brand__iexact=brand)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['brands'] = Product.objects.all().values_list('brand', flat=True).distinct()
        context.update({
            'categories': Category.objects.order_by('title').all(),
            'subcategories': SubCategory.objects.all(),
            'navbar_pages': Page.objects.filter(navbar=True),
            'totals': Cart.objects.all()
        })
        return context


class BrandView(ListView):
    model = Product
    # queryset = Product.objects.filter(is_active=True)
    template_name = 'all_product.html'
    context_object_name = 'allproduct_pages'
    paginate_by = 9

    def dispatch(self, request, *args, **kwargs):
        if request.user.role == "Patient":
            raise Http404()
        return super().dispatch(request, *args, **kwargs)

    # ordering = 'title'

    def get_queryset(self, *args, **kwargs):
        brand = self.kwargs.get('slug', None)
        if brand is None:
            queryset = super().get_queryset(*args, **kwargs)
        else:
            queryset = self.model.objects.filter(brand__iexact=brand)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        context.update({
            'categories': Category.objects.order_by('title').all(),
            'subcategories': SubCategory.objects.all(),
            'navbar_pages': Page.objects.filter(navbar=True),
            'totals': Cart.objects.all()
        })
        return context


class SingleProduct(BaseView):
    def get(self, request, single_slug):
        self.template_context['single_product'] = Product.objects.get(slug=single_slug)
        self.template_context['picked'] = Product.objects.order_by('?')[:4]
        self.template_context['comments'] = ProductReview.objects.all()
        return render(request, 'single-product.html', self.template_context)

    def post(self, request, single_slug):
        form = CommentForm(request.POST)
        if form.is_valid():
            product_review = form.save(commit=False)
            product_review.user = request.user
            product_review.product = Product.objects.get(slug=single_slug)
            product_review.save()

        return redirect('/')


class SearchView(BaseView):
    def get(self, request):
        q = request.GET.get('q', None)
        if not q:
            return redirect('/')

        self.template_context['search_results'] = Product.objects.filter(title__icontains=q)
        return render(request, 'search_result.html', self.template_context)


class SignUpView(BaseView):
    def get(self, request):
        self.template_context['form'] = SignUpForm()
        return render(request, 'registration/register.html', self.template_context)

    def post(self, request):
        form = SignUpForm(request.POST)
        if not form.is_valid():
            self.template_context['form'] = form
            return render(request, 'registration/register.html', self.template_context)

        form.save()
        return redirect('/accounts/login')


class CartView(LoginRequiredMixin, BaseView):
    def get(self, request):
        self.template_context['cart_items'] = Cart.objects.filter(user=request.user).order_by('-id')

        total_amount = 0
        for cart_item in self.template_context['cart_items']:
            total_amount += cart_item.total()

        self.template_context['total_amount'] = total_amount
        request.session['ta'] = total_amount

        return render(request, 'cart.html', self.template_context)

    def post(self, request):
        item = Cart()
        product = Product.objects.get(pk=request.POST.get('product_id'))
        user_carts = Cart.objects.filter(product=product, user=request.user)
        if user_carts.exists():
            user_cart = user_carts.first()
            user_cart.qty += int(request.POST.get('qty'))
            user_cart.save()
        else:
            item.product = product
            item.qty = request.POST.get('qty')
            item.user = request.user
            item.save()
        return redirect('/cart')


class CheckoutView(LoginRequiredMixin, BaseView):
    def get(self, request):
        self.template_context['cart_itms'] = Cart.objects.filter(user=request.user).order_by('-id')
        return render(request, 'checkout.html', self.template_context)


class RemoveCartView(BaseView):
    def get(self, request):
        # self.template_context['cart_ites'] = Cart.objects.filter(user=request.user).order_by('-id')
        return render(request, 'cart.html', self.template_context)

    def post(self, request):
        Cart.objects.get(pk=request.POST.get('id')).delete()
        return redirect('/cart')
