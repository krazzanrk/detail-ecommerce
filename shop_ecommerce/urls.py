from django.urls import path
from .views import *

app_name = 'shop_ecommerce'

urlpatterns = [
    path('', HomePage.as_view(), name='index'),
    path('search', SearchView.as_view(), name='search'),
    path('subcategory/<str:sub_slug>', SubCategoryView.as_view(), name='sub_wise'),
    path('product/', AllProduct.as_view(), name='product_wise'),
    path('singleproduct/<str:single_slug>', SingleProduct.as_view(), name='single'),
    path('register', SignUpView.as_view(), name='register'),
    path('cart', CartView.as_view(), name='cart'),
    path('checkout', CheckoutView.as_view(), name='checkout'),
    path('rem', RemoveCartView.as_view(), name='remove'),
    path('product/<str:slug>/', BrandView.as_view(), name='brand_wise'),

]
