from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from .models import ProductReview, Signup


class SignupForm(ModelForm):
    class Meta:
        model = Signup
        fields = ('name', 'email', 'password', 'confirm_password')


class SignUpForm(UserCreationForm):
    pass


class CommentForm(ModelForm):
    class Meta:
        model = ProductReview
        fields = ('comment', 'rating')
